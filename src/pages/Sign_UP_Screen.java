package pages;

import org.openqa.selenium.WebElement;

import io.appium.java_client.AppiumDriver;

public class Sign_UP_Screen extends BaseClass{

	public Sign_UP_Screen(AppiumDriver driver) {
		super(driver);
	}
	
	public static WebElement element=null;
	
	public static WebElement text_Header()
	{
		try {
			element = driver.findElementById("esselgroup.zeemedia.mystory:id/headerTV");
		} catch (Exception e) {
			element = driver.findElementById("//android.widget.TextView[@index='1' and @text='Sign Up']");
		}
		return element;
	}
	
	public static WebElement button_Back()
	{
		try {
			element = driver.findElementById("esselgroup.zeemedia.mystory:id/backTV");
		} catch (Exception e) {
			element = driver.findElementById("//android.widget.TextView[@index='0' and @text='Back']");
		}
		return element;
	}
	
	public static WebElement input_Name()
	{
		try {
			element = driver.findElementById("esselgroup.zeemedia.mystory:id/nameEditText");
			
		} catch (Exception e) {
			element = driver.findElementByXPath("//android.widget.EditText[@text='Name']");
		}
		return element;
	}
	
	public static WebElement input_Email()
	{
		try {
			element = driver.findElementById("esselgroup.zeemedia.mystory:id/emailEditText");
			
		} catch (Exception e) {
			element = driver.findElementByXPath("//android.widget.EditText[@text='Email']");
		}
		return element;
	}
	
	public static WebElement input_Password()
	{
		try {
			element = driver.findElementById("esselgroup.zeemedia.mystory:id/passwordEditText");
			
		} catch (Exception e) {
			element = driver.findElementByXPath("//android.widget.EditText[@index='2']");
		}
		return element;
	}
	
	public static WebElement input_MobileNumber()
	{
		try {
			element = driver.findElementById("esselgroup.zeemedia.mystory:id/mobileEditText");
			
		} catch (Exception e) {
			element = driver.findElementByXPath("//android.widget.EditText[@text='Mobile']");
		}
		return element;
	}
	
	public static WebElement button_SignUP()
	{
		try {
			element = driver.findElementById("esselgroup.zeemedia.mystory:id/signUpButton");
			
		} catch (Exception e) {
			element = driver.findElementByXPath("//android.widget.Button[@text='Sign Up']");
		}
		return element;
	}

}
