package pages;

import org.openqa.selenium.WebElement;

import io.appium.java_client.AppiumDriver;

public class Sign_In_Screen extends BaseClass{

	public Sign_In_Screen(AppiumDriver driver) {
		super(driver);
	}
	
	public static WebElement element=null;
	
	public static WebElement text_Header()
	{
		try {
			element = driver.findElementById("esselgroup.zeemedia.mystory:id/headerTV");
		} catch (Exception e) {
			element = driver.findElementById("//android.widget.TextView[@index='1' and @text='Sign In']");
		}
		return element;
	}
	public static WebElement button_Back()
	{
		try {
			element = driver.findElementById("esselgroup.zeemedia.mystory:id/backTV");
		} catch (Exception e) {
			element = driver.findElementById("//android.widget.TextView[@index='0' and @text='Back']");
		}
		return element;
	}
	
	public static WebElement input_UserName()
	{
		try {
			element = driver.findElementById("esselgroup.zeemedia.mystory:id/userNameEditText");
			
		} catch (Exception e) {
			element = driver.findElementByXPath("//android.widget.EditText[@text='Email']");
		}
		return element;
	}
	
	public static WebElement input_Password()
	{
		try {
			element = driver.findElementById("esselgroup.zeemedia.mystory:id/passwordEditText");
			
		} catch (Exception e) {
			element = driver.findElementByXPath("//android.widget.EditText[@index='1']");
		}
		return element;
	}
	
	public static WebElement button_SIGNIN()
	{
		try {
			element = driver.findElementById("esselgroup.zeemedia.mystory:id/signinButton");
		} catch (Exception e) {
			element = driver.findElementByXPath("//android.widget.Button[@index='1' and @text='Sign In']");
		}return element;
	}
	
	public static WebElement lnk_SignUP()
	{
		try {
			element = driver.findElementById("esselgroup.zeemedia.mystory:id/signupTVButton");
		} catch (Exception e) {
			element = driver.findElementByXPath("//android.widget.TextView[@index='0' and @text='Sign Up']");
		}return element;
	}
	
	public static WebElement lnk_ForgotPassword()
	{
		try {
			element = driver.findElementById("esselgroup.zeemedia.mystory:id/forgotPassTVButton");
		} catch (Exception e) {
			element = driver.findElementByXPath("//android.widget.TextView[@index='1' and @text='Forgot Password']");
		}return element;
	}
	
	public static WebElement button_SignIN_FB()
	{
		try {
			element = driver.findElementById("esselgroup.zeemedia.mystory:id/signInFbButton");
		} catch (Exception e) {
			element = driver.findElementByXPath("//android.widget.Button[@index='0' and @text='Sign In with Facebook']");
		}return element;
	}
	
	public static WebElement button_SignIN_Google()
	{
		try {
			element = driver.findElementById("esselgroup.zeemedia.mystory:id/signInTwitterButton");
		} catch (Exception e) {
			element = driver.findElementByXPath("//android.widget.Button[@index='1' and @text='Sign In with Google']");
		}return element;
	}
	
	/*public static WebElement button_SignIN_Google()
	{
		try {
			element = driver.findElementById("esselgroup.zeemedia.mystory:id/signInFbButton");
		} catch (Exception e) {
			element = driver.findElementByXPath("//android.widget.Button[@index='0' and @text='Sign In with Facebook']");
		}return element;
	}*/
	
	

}
