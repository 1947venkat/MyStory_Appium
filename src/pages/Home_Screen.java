package pages;




import io.appium.java_client.AppiumDriver;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;

public class Home_Screen extends BaseClass {

	public static WebElement element ;
	
	public Home_Screen(AppiumDriver driver) {
		super(driver);
	}
	
	public static WebElement button_LeftNavigation_Bar()
	{
		element = driver.findElementByXPath("//android.widget.ImageButton[@index='0']"); 
		return element;
	}
	
	public static WebElement button_myStory_right ()
	{
		element = driver.findElement(By.id("esselgroup.zeemedia.mystory:id/rightMenuIB"));
//		element = driver.findElementById("esselgroup.zeemedia.mystory:id/rightMenuIB"); 
		return element;
	}
	
	public static WebElement button_favourites_id ()
	{
		element = driver.findElementById("esselgroup.zeemedia.mystory:id/favoritesButton"); 
		return element;
	}
	public static WebElement myPosts_button_id ()
	{
		element = driver.findElementById("esselgroup.zeemedia.mystory:id/myFeedsButton"); 
		return element;
	}
	
	public static WebElement lnk_UploadStory()
	{
		try {
			element = driver.findElementByName("UPLOAD STORY"); 
		}catch (Exception e) {
			element =driver.findElementByXPath("//android.widget.TextView[@text='UPLOAD STORY']");
		}
		return element;
	}
	public static WebElement lnk_feeds ()
	{
		try {
			element = driver.findElementByName("FEEDS"); 
		}catch (Exception e) {
			element =driver.findElementByXPath("//android.widget.TextView[@text='FEEDS']");
		}
		return element;
	}
	
	public static WebElement lnk_MyPosts()
	{
		try {
			element = driver.findElementByName("MY POSTS"); 
		}catch (Exception e) {
			element =driver.findElementByXPath("//android.widget.TextView[@text='MY POSTS']");
		}
		return element;
	}
	public static WebElement lnk_Favourites()
	{
		try {
			element = driver.findElementByName("FAVOURITES"); 
		}catch (Exception e) {
			element =driver.findElementByXPath("//android.widget.TextView[@text='FAVOURITES']");
		}
		return element;
	}
	
	public static WebElement lnk_MyActivities()
	{
		try {
			element = driver.findElementByName("MY ACTIVITIES"); 
		}catch (Exception e) {
			element =driver.findElementByXPath("//android.widget.TextView[@text='MY ACTIVITIES']");
		}
		return element;
	}
	
	public static WebElement lnk_Settings()
	{
		try {
			element = driver.findElementByName("SETTINGS"); 
		}catch (Exception e) {
			element =driver.findElementByXPath("//android.widget.TextView[@text='SETTINGS']");
		}
		return element;
	}
	
	public static WebElement lnk_ChangePassword()
	{
		try {
			element = driver.findElementByName("CHANGE PASSWORD"); 
		}catch (Exception e) {
			element =driver.findElementByXPath("//android.widget.TextView[@text='CHANGE PASSWORD']");
		}
		return element;
	}
	
	public static WebElement lnk_SIGNIN()
	{
		element = driver.findElementByName("SIGN IN"); 
		return element;
	}
	
	public static WebElement lnk_SignOUT()
	{
		element = driver.findElementByName("SIGN OUT"); 
		return element;
	}
	public static WebElement logo_LogIN_Header()
	{
		element = driver.findElementById("esselgroup.zeemedia.mystory:id/loginLogoIV"); 
		return element;
	}
	public static WebElement text_UserName()
	{
		element = driver.findElementById("esselgroup.zeemedia.mystory:id/userName"); 
		return element;
	}
	
	public static WebElement logo_User()
	{
		element = driver.findElementById("esselgroup.zeemedia.mystory:id/logoutUserImage"); 
		return element;
	}
}
