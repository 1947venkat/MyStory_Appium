package pages;

import org.openqa.selenium.WebElement;

import io.appium.java_client.AppiumDriver;

public class Dashboard_Screen extends BaseClass{

	public Dashboard_Screen(AppiumDriver driver) {
		super(driver);
	}
	public static WebElement element=null;
	
	public static WebElement button_Reset()
	{
		element = driver.findElementById("esselgroup.zeemedia.mystory:id/chatClearIV");
		return element;
	}
	
	public static WebElement text_Heading()
	{
		element = driver.findElementById("esselgroup.zeemedia.mystory:id/chatbotHeaderTextView");
		return element;
	}
	
	
	public static WebElement button_Back()
	{
		element = driver.findElementById("esselgroup.zeemedia.mystory:id/chatBotCancelIV");
		return element;
	}
	public static WebElement input_typeHere()
	{
		element = driver.findElementByXPath("//android.widget.EditText[@resource-id='esselgroup.zeemedia.mystory:id/chatEditBox']");
		return element;
	}
	
	public static WebElement button_Submit()
	{
		element = driver.findElementById("esselgroup.zeemedia.mystory:id/chatMessageSubmitButton");
		return element;
	}
	
	public static WebElement button_attachment_ICON()
	{
		element = driver.findElementById("esselgroup.zeemedia.mystory:id/chatbotAttachmentIV");
		return element;
	}
	public static WebElement button_uploadGallery_ICON()
	{
		element = driver.findElementByXPath("//Upload from gallery[@resource-id='esselgroup.zeemedia.mystory:id/upload_gallery']");
		return element;
	}
	public static WebElement button_capturePhoto_ICON()
	{
		element = driver.findElementByXPath("//Capture photo[@resource-id='esselgroup.zeemedia.mystory:id/capture_camera']");
		return element;
	}
	
	public static WebElement button_selectLocation_ICON()
	{
		element = driver.findElementByXPath("//Select location[@resource-id='esselgroup.zeemedia.mystory:id/select_location']");
		return element;
	}
	
	public static WebElement text_FirstMessage(String elementText)
	{
		element = driver.findElementByXPath("//esselgroup.zeemedia.mystory:id/text[@text='Hi zooly, hope you are doing great!']");
		return element;
	}
	
	
	
}
