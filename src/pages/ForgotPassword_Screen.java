package pages;

import org.openqa.selenium.WebElement;

import io.appium.java_client.AppiumDriver;

public class ForgotPassword_Screen extends BaseClass{

	public ForgotPassword_Screen(AppiumDriver driver) {
		super(driver);
	}
	
	public static WebElement element=null;
	
	public static WebElement text_Header()
	{
		try {
			element = driver.findElementById("esselgroup.zeemedia.mystory:id/headerTV");
		} catch (Exception e) {
			element = driver.findElementById("//android.widget.TextView[@index='1' and @text='Forgot Password']");
		}
		return element;
	}
	public static WebElement button_Back()
	{
		try {
			element = driver.findElementById("esselgroup.zeemedia.mystory:id/backTV");
		} catch (Exception e) {
			element = driver.findElementById("//android.widget.TextView[@index='0' and @text='Back']");
		}
		return element;
	}
	
	public static WebElement input_Email()
	{
		try {
			element = driver.findElementById("esselgroup.zeemedia.mystory:id/emailEditText");
			
		} catch (Exception e) {
			element = driver.findElementByXPath("//android.widget.EditText[@text='Email']");
		}
		return element;
	}
	
	public static WebElement button_Submit()
	{
		try {
			element = driver.findElementById("esselgroup.zeemedia.mystory:id/submit");
		} catch (Exception e) {
			element = driver.findElementByXPath("//android.widget.Button[@text='Submit']");
		}return element;
	}
	
	

}
