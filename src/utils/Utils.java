package utils;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import pages.BaseClass;

public class Utils  extends BaseClass{

	public Utils(AppiumDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}
	
	public static void launchApp() throws MalformedURLException
	{
		DesiredCapabilities caps = new DesiredCapabilities();
		caps.setCapability("platformVersion", "5.1");
		caps.setCapability("platformName", "Android");
		caps.setCapability("deviceName", "lenovo");
		caps.setCapability("appPackage","esselgroup.zeemedia.mystory");
		caps.setCapability("appWaitPackage","esselgroup.zeemedia.mystory");
		caps.setCapability("appActivity","esselgroup.zeemedia.mystory.activities.SplashActivity");
		caps.setCapability("appWaitActivity","esselgroup.zeemedia.mystory.activities.SplashActivity");
		driver = new AndroidDriver(new URL("http://127.0.0.1:4723/wd/hub"), caps);
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
	}

	public static void waitForMyTime(long timeSec)
	{
		driver.manage().timeouts().implicitlyWait(timeSec, TimeUnit.SECONDS);
	}
	
	public static void waitforElement_Click(WebElement element,int timeSec)
	{
		try {
			WebDriverWait wait = new WebDriverWait(driver,timeSec);
			wait.until(ExpectedConditions.elementToBeClickable(element));
		} catch (Exception e) {
			System.out.println("the Exception is ::"+e.getMessage());
		}
	}
	
	public static void waitforElement_Present(WebElement element,int timeSec)
	{
		try {
			WebDriverWait wait = new WebDriverWait(driver,timeSec);
			wait.until(ExpectedConditions.visibilityOf(element));
		} catch (Exception e) {
			System.out.println("the Exception is ::"+e.getMessage());
		}
	}
	
	public static void clickElement(WebElement element)
	{
		try {
			if(element != null)
			{
				if(element.isDisplayed())
				{
					element.click();
				}else{
					System.out.println("element is not dispalyed");
				}
			}else{
				System.out.println("element is null");
			}
		} catch (Exception e) {
			System.out.println("the Exception details are :: "+e.getMessage());
		}
	}
	
	public static void setText(WebElement element, String data)
	{
		try {
			element.clear();
			element.sendKeys(data);
		} catch (Exception e) {
			System.out.println("the Exception details are ::"+e.getMessage());
		}
	}
}
