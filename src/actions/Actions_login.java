package actions;

import pages.Dashboard_Screen;
import pages.Home_Screen;
import pages.Sign_In_Screen;
import utils.Utils;

public class Actions_login {

	public static void signINFrom_Dashboard()
	{
		Utils.waitforElement_Present(Home_Screen.button_myStory_right(), 10);
		Utils.clickElement(Home_Screen.button_myStory_right());
		Utils.waitforElement_Present(Dashboard_Screen.input_typeHere(), 10);
		Utils.clickElement(Dashboard_Screen.input_typeHere());
		
		Actions_login.sign_IN();
	}
	
	public static void sign_IN()
	{
		Utils.waitforElement_Present(Sign_In_Screen.text_Header(), 10);
		Utils.setText(Sign_In_Screen.input_UserName(), "zooly@gmail.com");
		Utils.setText(Sign_In_Screen.input_Password(), "ray@1234");
		Utils.clickElement(Sign_In_Screen.button_SIGNIN());
	}
}
